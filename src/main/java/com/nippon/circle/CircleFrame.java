/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.circle;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 *
 * @author Nippon
 */
public class CircleFrame {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Circle");
        frame.setSize(300, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblr = new JLabel("Radius", JLabel.CENTER);
        lblr.setSize(50, 20);
        lblr.setLocation(5, 5);
        lblr.setBackground(Color.WHITE);
        lblr.setOpaque(true);
        frame.add(lblr);

        JTextField txtr = new JTextField();
        txtr.setSize(50, 20);
        txtr.setLocation(60, 5);
        frame.add(txtr);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        frame.add(btnCalculate);

        final JLabel lblResult = new JLabel("Circle radius = ??? | area = ??? | parimeter = ??? ");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        frame.add(lblResult);

        //Event Driven
        btnCalculate.addActionListener(new ActionListener() { //Anonymous class
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    //1.pull text from txtr->>strr
                    String strr = txtr.getText();
                    //2.change strr ->>r : double parseDouble
                    double r = Double.parseDouble(strr); //NumbetFormatException
                    //3.instance object Circle(r)->> circle
                    Circle circle = new Circle(r);
                    //4.update lblResult , input from circle to show
                    lblResult.setText("Circle r = " + String.format("%.2f", circle.getR())
                            + " area = " + String.format("%.2f", circle.calArea())
                            + " perimeter = " + String.format("%.2f", circle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error : Please input Number", "Error",
                            JOptionPane.ERROR_MESSAGE);
                    txtr.setText("");
                    txtr.requestFocus();
                }
            }

        });
        frame.setVisible(true);
    }
}
