/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.circle;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 *
 * @author Nippon
 */
public class SquareFrame {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Square");
        frame.setSize(300, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lbls = new JLabel("Side :  ", JLabel.CENTER);
        lbls.setSize(50, 20);
        lbls.setLocation(5, 5);
        lbls.setBackground(Color.WHITE);
        lbls.setOpaque(true);
        frame.add(lbls);

        JTextField txts = new JTextField();
        txts.setSize(50, 20);
        txts.setLocation(60, 5);
        frame.add(txts);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        frame.add(btnCalculate);

        final JLabel lblResult = new JLabel("Square side = ??? | area = ??? | parimeter = ??? ");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.GREEN);
        lblResult.setOpaque(true);
        frame.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strs = txts.getText();
                    double s = Double.parseDouble(strs); //NumbetFormatException
                    Square square = new Square(s);
                    lblResult.setText("Square s = " + String.format("%.2f", square.getS())
                            + " area = " + String.format("%.2f", square.calArea())
                            + " perimeter = " + String.format("%.2f", square.calPerimeter()));
                    txts.setText("");
                    txts.requestFocus();
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error : Please input Number", "Error",
                            JOptionPane.ERROR_MESSAGE);
                    txts.setText("");
                    txts.requestFocus();
                }
            }

        });

        frame.setVisible(true);
    }
}
