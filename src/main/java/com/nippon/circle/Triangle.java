/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.circle;

/**
 *
 * @author Nippon
 */
public class Triangle extends Shape {

    private double b;

    public Triangle(double b) {
        super("Triangle");
        this.b = b;

    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    @Override
    public double calArea() {
        return 0.5 * b * b;
    }

    @Override
    public double calPerimeter() {
        return b + b + b;
    }

}
