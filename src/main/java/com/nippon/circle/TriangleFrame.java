/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.circle;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Nippon
 */
public class TriangleFrame {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Triangle");
        frame.setSize(300, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblb = new JLabel("Base :  ", JLabel.CENTER);
        lblb.setSize(50, 20);
        lblb.setLocation(5, 5);
        lblb.setBackground(Color.WHITE);
        lblb.setOpaque(true);
        frame.add(lblb);

        JTextField txtb = new JTextField();
        txtb.setSize(50, 20);
        txtb.setLocation(60, 5);
        frame.add(txtb);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        frame.add(btnCalculate);

        final JLabel lblResult = new JLabel("Triangle base = ??? | area = ??? | parimeter = ??? ");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.GREEN);
        lblResult.setOpaque(true);
        frame.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strb = txtb.getText();
                    double b = Double.parseDouble(strb); //NumbetFormatException
                    Triangle triangle = new Triangle(b);
                    lblResult.setText("Triangle base = " + String.format("%.2f", triangle.getB())
                            + " area = " + String.format("%.2f", triangle.calArea())
                            + " perimeter = " + String.format("%.2f", triangle.calPerimeter()));
                    txtb.setText("");
                    txtb.requestFocus();
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error : Please input Number", "Error",
                            JOptionPane.ERROR_MESSAGE);
                    txtb.setText("");
                    txtb.requestFocus();
                }
            }
        });

        frame.setVisible(true);
    }
}
