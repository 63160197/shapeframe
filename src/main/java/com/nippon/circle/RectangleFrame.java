/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.circle;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 *
 * @author Nippon
 */
public class RectangleFrame {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Rectangle");
        frame.setSize(300, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblwidth = new JLabel("Width   : ", JLabel.CENTER);
        lblwidth.setSize(50, 20);
        lblwidth.setLocation(5, 5);
        lblwidth.setBackground(Color.WHITE);
        lblwidth.setOpaque(true);
        frame.add(lblwidth);

        JLabel lbllength = new JLabel("Length : ", JLabel.CENTER);
        lbllength.setSize(50, 20);
        lbllength.setLocation(5, 30);
        lbllength.setBackground(Color.WHITE);
        lbllength.setOpaque(true);
        frame.add(lbllength);

        JTextField txtw = new JTextField();
        txtw.setSize(50, 20);
        txtw.setLocation(60, 5);
        frame.add(txtw);

        JTextField txtl = new JTextField();
        txtl.setSize(50, 20);
        txtl.setLocation(60, 30);
        frame.add(txtl);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(150, 20);
        frame.add(btnCalculate);

        final JLabel lblResult = new JLabel("Rectangle area = ??? | parimeter = ??? ");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300, 50);
        lblResult.setLocation(0, 70);
        lblResult.setBackground(Color.LIGHT_GRAY);
        lblResult.setOpaque(true);
        frame.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strw = txtw.getText();
                    String strl = txtl.getText();
                    double x = Double.parseDouble(strw);
                    double y = Double.parseDouble(strl);
                    double times = 0;
                    times = x * y;
                    double perimeter = 0;
                    perimeter = (2 * (x + y));
                    Rectangle rectangle = new Rectangle(x, y);
                    lblResult.setText("Rectangle area = " + String.format("%.2f", times)
                            + " perimeter = " + String.format("%.2f", rectangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error : Please input Number", "Error",
                            JOptionPane.ERROR_MESSAGE);
                    txtw.setText("");
                    txtl.setText("");
                    txtw.requestFocus();

                }
            }

        });
        frame.setVisible(true);
    }
}
