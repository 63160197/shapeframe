/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.circle;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Nippon
 */
public class CircleFrame2 extends JFrame {

    JLabel lblr;
    JTextField txtr;
    JButton btnCalculate;
    JLabel lblResult;

    public CircleFrame2() {
        super("Circle");
        this.setSize(300, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblr = new JLabel("Radius", JLabel.CENTER);
        lblr.setSize(50, 20);
        lblr.setLocation(5, 5);
        lblr.setBackground(Color.WHITE);
        lblr.setOpaque(true);
        this.add(lblr);

        txtr = new JTextField();
        txtr.setSize(50, 20);
        txtr.setLocation(60, 5);
        this.add(txtr);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);

        lblResult = new JLabel("Circle radius = ??? | area = ??? | parimeter = ??? ");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        this.add(lblResult);

        //Event Driven
        btnCalculate.addActionListener(new ActionListener() { //Anonymous class
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    //1.pull text from txtr->>strr
                    String strr = txtr.getText();
                    //2.change strr ->>r : double parseDouble
                    double r = Double.parseDouble(strr); //NumbetFormatException
                    //3.instance object Circle(r)->> circle
                    Circle circle = new Circle(r);
                    //4.update lblResult , input from circle to show
                    lblResult.setText("Circle r = " + String.format("%.2f", circle.getR())
                            + " area = " + String.format("%.2f", circle.calArea())
                            + " perimeter = " + String.format("%.2f", circle.calPerimeter()));
                    txtr.setText("");
                    txtr.requestFocus();
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(CircleFrame2.this, "Error : Please input Number", "Error",
                            JOptionPane.ERROR_MESSAGE);
                    txtr.setText("");
                    txtr.requestFocus();
                }
            }

        });
    }

    public static void main(String[] args) {
        CircleFrame2 frame = new CircleFrame2();
        frame.setVisible(true);
    }
}
